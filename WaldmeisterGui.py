from enum import Enum
import random
import pygame
import numpy as np
from waldmeister.keras.NNet import NNetWrapper as NNet
from utils import dotdict
from MCTS import MCTS
from waldmeister.WaldmeisterGame import WaldmeisterGame

from pygame.locals import (
    RLEACCEL,
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    QUIT
)

from waldmeister.WaldmeisterPlayer import RandomPlayer

class TurnState(Enum):
    SELECT_FROM_STONE = 1
    SELECT_TO_STONE = 2
    SELECT_STONE_TYPE = 3

class GameSprite(pygame.sprite.Sprite):
    def __init__(self, path : str) -> None:
        super().__init__()
        self.surf = pygame.image.load(path).convert_alpha()
        self.surf.set_colorkey((255, 255, 255), pygame.SRCALPHA)
        self.rect = self.surf.get_rect()


def getStoneSpriteFromNum(number: int):
     match number:
        case 3:
            return GameSprite("./game_pictures/Klein_GrünGelb.png")
        case 4:
            return GameSprite("./game_pictures/MittelGroß_GelbGrün.png")
        case 5:
            return GameSprite("./game_pictures/Groß_GelbGrün.png")
        case 6:
            return GameSprite("./game_pictures/Klein_BlattGrün.png")
        case 7:
            return GameSprite("./game_pictures/MittelGroß_BlattGrün.png")
        case 8:
            return GameSprite("./game_pictures/Groß_BlattGrün.png")
        case 9:
            return GameSprite("./game_pictures/Klein_DunkelGrün.png")
        case 10:
            return GameSprite("./game_pictures/MittelGroß_DunkelGrün.png")
        case 11:
            return GameSprite("./game_pictures/Groß_DunkelGrün.png")
        case _:
            return None         
pygame.init()

SCREEN_WIDTH = 1410
SCREEN_HEIGHT = 900

BOARD_POS = (0, 80)
HITBOX_WIDTH = 65
HITBOX_HEIGHT = 43

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

font = pygame.font.SysFont("arial", 50)
scoreFont = pygame.font.SysFont("noto mono", 40)


boardSprite = GameSprite("./game_pictures/Waldmeister_Board_Tilted_View_Hopefully.png")

running : bool= True

game = WaldmeisterGame()             # Standardmäßig spielt man selbst als colorPlayer.
#game = WaldmeisterGame(gameGoal=-1) # Wenn man als heightPlayer spielen möchte.
player = RandomPlayer(game).play
n1 = NNet(game)
n1.load_checkpoint('./temp/', 'best.h5') # best.h5 ist die beste Version der trainierten KI, also das letzte accept new model.

args1 = dotdict({'numMCTSSims': 50, 'cpuct':1.0}) # numMCTSSims ist die Anzahl der möglichen Züge die der Algorithmus für einen Zug durchsuchen und bewerten soll.
                                                  # Der beste Zug soll daraufhin ausgewählt und durchgeführt werden.
mcts1 = MCTS(game, n1, args1)
n1p = lambda x, p: np.argmax(mcts1.getActionProb(x, p, temp=0))

curPlayer = 1
board = game.getInitBoard()         

state : TurnState = TurnState.SELECT_TO_STONE

gameMoveX : int
gameMoveY : int
gameMoveNewX : int
gameMoveNewY : int
gameMoveStoneType : int

turnNumber = 1
groupCounts = game.getGroupCounts(board)
heightStrings = ["short", "medium", "tall"]
colorStrings = ["yellow-green", "leaf-green", "dark-green"]  
 
stoneIcons : list = list()

for i in range(3, 12):
    stoneSprite = getStoneSpriteFromNum(i)
    stoneSprite.surf = pygame.transform.scale_by(stoneSprite.surf, 0.5 - i % 3 * 0.1)
    stoneIcons.append(stoneSprite)

# Die Werte der einzelnen Tupel sind so zu verstehen -> x pos box, y pos box, breite, höhe, x pos zeichnen, y pos zeichnen
# Aufgrund des Zeichnens in Krita waren viele Steine nur ungleichmäßig platzierbar, weshalb es hier nötig war jedes Feld einzeln festzulegen. 
stoneSlotHitboxes = [(72, BOARD_POS[1] + 310, HITBOX_WIDTH, HITBOX_HEIGHT, 72 , BOARD_POS[1] + 350),
                     (167, BOARD_POS[1] + 265, HITBOX_WIDTH, HITBOX_HEIGHT, 162, BOARD_POS[1] + 310),
                     (252, BOARD_POS[1] + 225, HITBOX_WIDTH, HITBOX_HEIGHT, 252, BOARD_POS[1] + 270),
                     (342, BOARD_POS[1] + 180, HITBOX_WIDTH, HITBOX_HEIGHT, 340, BOARD_POS[1] + 230),
                     (427, BOARD_POS[1] + 140, HITBOX_WIDTH, HITBOX_HEIGHT, 425, BOARD_POS[1] + 190),
                     (509, BOARD_POS[1] + 100, HITBOX_WIDTH, HITBOX_HEIGHT, 507, BOARD_POS[1] + 150),
                     (594, BOARD_POS[1] + 60, HITBOX_WIDTH, HITBOX_HEIGHT, 588, BOARD_POS[1] + 110),
                     (677, BOARD_POS[1] + 17, HITBOX_WIDTH, HITBOX_HEIGHT, 667, BOARD_POS[1] + 70),
                     
                     (167, BOARD_POS[1] + 350, HITBOX_WIDTH, HITBOX_HEIGHT, 152, BOARD_POS[1] + 390),
                     (252, BOARD_POS[1] + 305, HITBOX_WIDTH, HITBOX_HEIGHT, 242, BOARD_POS[1] + 350),
                     (342, BOARD_POS[1] + 265, HITBOX_WIDTH, HITBOX_HEIGHT, 330, BOARD_POS[1] + 310),
                     (427, BOARD_POS[1] + 220, HITBOX_WIDTH, HITBOX_HEIGHT, 416, BOARD_POS[1] + 270),
                     (509, BOARD_POS[1] + 180, HITBOX_WIDTH, HITBOX_HEIGHT, 502, BOARD_POS[1] + 230),
                     (594, BOARD_POS[1] + 140, HITBOX_WIDTH, HITBOX_HEIGHT, 585, BOARD_POS[1] + 190),
                     (677, BOARD_POS[1] + 100, HITBOX_WIDTH, HITBOX_HEIGHT, 665, BOARD_POS[1] + 150),
                     (760, BOARD_POS[1] + 57, HITBOX_WIDTH, HITBOX_HEIGHT, 748, BOARD_POS[1] + 108),
                     
                     (232, BOARD_POS[1] + 390, HITBOX_WIDTH, HITBOX_HEIGHT, 232, BOARD_POS[1] + 432),
                     (325, BOARD_POS[1] + 350, HITBOX_WIDTH, HITBOX_HEIGHT, 325, BOARD_POS[1] + 390),
                     (411, BOARD_POS[1] + 310, HITBOX_WIDTH, HITBOX_HEIGHT, 411, BOARD_POS[1] + 350),
                     (495, BOARD_POS[1] + 270, HITBOX_WIDTH, HITBOX_HEIGHT, 502, BOARD_POS[1] + 310),
                     (581, BOARD_POS[1] + 230, HITBOX_WIDTH, HITBOX_HEIGHT, 585, BOARD_POS[1] + 270),
                     (665, BOARD_POS[1] + 190, HITBOX_WIDTH, HITBOX_HEIGHT, 665, BOARD_POS[1] + 230),
                     (750, BOARD_POS[1] + 150, HITBOX_WIDTH, HITBOX_HEIGHT, 747, BOARD_POS[1] + 190),
                     (830, BOARD_POS[1] + 107, HITBOX_WIDTH, HITBOX_HEIGHT, 827, BOARD_POS[1] + 146),
                     
                     (313, BOARD_POS[1] + 430, HITBOX_WIDTH, HITBOX_HEIGHT, 315, BOARD_POS[1] + 480),
                     (403, BOARD_POS[1] + 390, HITBOX_WIDTH, HITBOX_HEIGHT, 406, BOARD_POS[1] + 435),
                     (493, BOARD_POS[1] + 350, HITBOX_WIDTH, HITBOX_HEIGHT, 495, BOARD_POS[1] + 390),
                     (583, BOARD_POS[1] + 300, HITBOX_WIDTH, HITBOX_HEIGHT, 582, BOARD_POS[1] + 347),
                     (673, BOARD_POS[1] + 260, HITBOX_WIDTH, HITBOX_HEIGHT, 665, BOARD_POS[1] + 304),
                     (760, BOARD_POS[1] + 225, HITBOX_WIDTH, HITBOX_HEIGHT, 749, BOARD_POS[1] + 268),
                     (850, BOARD_POS[1] + 185, HITBOX_WIDTH, HITBOX_HEIGHT, 830, BOARD_POS[1] + 225),
                     (920, BOARD_POS[1] + 143, HITBOX_WIDTH, HITBOX_HEIGHT, 907, BOARD_POS[1] + 185),
                     
                     (385, BOARD_POS[1] + 480, HITBOX_WIDTH, HITBOX_HEIGHT, 400, BOARD_POS[1] + 530),
                     (476, BOARD_POS[1] + 430, HITBOX_WIDTH, HITBOX_HEIGHT, 491, BOARD_POS[1] + 480),
                     (565, BOARD_POS[1] + 395, HITBOX_WIDTH, HITBOX_HEIGHT, 580, BOARD_POS[1] + 435),
                     (652, BOARD_POS[1] + 350, HITBOX_WIDTH, HITBOX_HEIGHT, 667, BOARD_POS[1] + 390),
                     (735, BOARD_POS[1] + 300, HITBOX_WIDTH, HITBOX_HEIGHT, 750, BOARD_POS[1] + 347),
                     (819, BOARD_POS[1] + 260, HITBOX_WIDTH, HITBOX_HEIGHT, 834, BOARD_POS[1] + 304),
                     (900, BOARD_POS[1] + 220, HITBOX_WIDTH, HITBOX_HEIGHT, 915, BOARD_POS[1] + 264),
                     (977, BOARD_POS[1] + 180, HITBOX_WIDTH, HITBOX_HEIGHT, 994, BOARD_POS[1] + 225),
                     
                     (485, BOARD_POS[1] + 521, HITBOX_WIDTH, HITBOX_HEIGHT, 487, BOARD_POS[1] + 571),
                     (575, BOARD_POS[1] + 475, HITBOX_WIDTH, HITBOX_HEIGHT, 578, BOARD_POS[1] + 525),
                     (660, BOARD_POS[1] + 427, HITBOX_WIDTH, HITBOX_HEIGHT, 667, BOARD_POS[1] + 477),
                     (750, BOARD_POS[1] + 387, HITBOX_WIDTH, HITBOX_HEIGHT, 754, BOARD_POS[1] + 432),
                     (833, BOARD_POS[1] + 344, HITBOX_WIDTH, HITBOX_HEIGHT, 839, BOARD_POS[1] + 389),
                     (913, BOARD_POS[1] + 300, HITBOX_WIDTH, HITBOX_HEIGHT, 920, BOARD_POS[1] + 347),
                     (993, BOARD_POS[1] + 259, HITBOX_WIDTH, HITBOX_HEIGHT, 1000, BOARD_POS[1] + 304),
                     (1073, BOARD_POS[1] + 219, HITBOX_WIDTH, HITBOX_HEIGHT, 1080, BOARD_POS[1] + 264),
                     
                     (576, BOARD_POS[1] + 569, HITBOX_WIDTH, HITBOX_HEIGHT, 576, BOARD_POS[1] + 619),
                     (667, BOARD_POS[1] + 521, HITBOX_WIDTH, HITBOX_HEIGHT, 667, BOARD_POS[1] + 571),
                     (756, BOARD_POS[1] + 474, HITBOX_WIDTH, HITBOX_HEIGHT, 756, BOARD_POS[1] + 524),
                     (843, BOARD_POS[1] + 427, HITBOX_WIDTH, HITBOX_HEIGHT, 843, BOARD_POS[1] + 477),
                     (926, BOARD_POS[1] + 382, HITBOX_WIDTH, HITBOX_HEIGHT, 926, BOARD_POS[1] + 432),
                     (1010, BOARD_POS[1] + 339, HITBOX_WIDTH, HITBOX_HEIGHT, 1010, BOARD_POS[1] + 389),
                     (1090, BOARD_POS[1] + 297, HITBOX_WIDTH, HITBOX_HEIGHT, 1090, BOARD_POS[1] + 347),
                     (1170, BOARD_POS[1] + 254, HITBOX_WIDTH, HITBOX_HEIGHT, 1170, BOARD_POS[1] + 304),
                     
                     (667, BOARD_POS[1] + 618, HITBOX_WIDTH, HITBOX_HEIGHT, 667, BOARD_POS[1] + 668),
                     (758, BOARD_POS[1] + 569, HITBOX_WIDTH, HITBOX_HEIGHT, 758, BOARD_POS[1] + 618),
                     (847, BOARD_POS[1] + 520, HITBOX_WIDTH, HITBOX_HEIGHT, 847, BOARD_POS[1] + 570),
                     (933, BOARD_POS[1] + 472, HITBOX_WIDTH, HITBOX_HEIGHT, 933, BOARD_POS[1] + 522),
                     (1017, BOARD_POS[1] + 426, HITBOX_WIDTH, HITBOX_HEIGHT, 1017, BOARD_POS[1] + 476),
                     (1100, BOARD_POS[1] + 382, HITBOX_WIDTH, HITBOX_HEIGHT, 1100, BOARD_POS[1] + 432),
                     (1182, BOARD_POS[1] + 338, HITBOX_WIDTH, HITBOX_HEIGHT, 1182, BOARD_POS[1] + 388),
                     (1260, BOARD_POS[1] + 297, HITBOX_WIDTH, HITBOX_HEIGHT, 1260, BOARD_POS[1] + 347),]

# Hitboxen für die Icons der einzelnen Spielsteine in der GUI.
stoneIconHitboxes = []
for y in range(3):       
        for x in range(3):
            stoneIconHitboxes.append((1160 + x * 80, 20 + y * 50, 80, 50))       

while running:
    # Die KI führt ihren Zug durch, wenn sie an der Reihe ist und das Spiel noch nicht vorbei ist.
    if curPlayer == -1 and game.getGameEnded(board, curPlayer) == 0:
        action = n1p(board, curPlayer)
        board, curPlayer = game.getNextState(board, curPlayer, action)
        turnNumber += 1
        game.display(board)
    
    for event in pygame.event.get():
        # Durch einen Klick mit der Maus wird die Position auf dem board ermittelt.
        if event.type == pygame.MOUSEBUTTONDOWN:
            mouseX, mouseY = pygame.mouse.get_pos()
            if 0 <= mouseX < SCREEN_WIDTH and 0 <= mouseY < SCREEN_HEIGHT:
                index : int = 0
                boardX : int = -1
                boardY : int = -1
                curHitbox = ()
                for hitbox in stoneSlotHitboxes:
                    if mouseX >= hitbox[0] and mouseX <= (hitbox[0] + hitbox[2]) and mouseY >= hitbox[1] and mouseY <= (hitbox[1] + hitbox[3]):
                        boardX = index % 8
                        boardY = index // 8
                        curHitbox = hitbox                        
                        break
                    index += 1       
                #print(boardX, boardY) 
                # Falls ein Klick die Hitbox eines Feldes nicht getroffen hat, soll sich der state nicht ändern.
                # Da es sich bei dem Klick um eine ungültige Position auf dem board handelt, soll nichts passieren, wenn nach dem Klick einen Stein zum Platzieren ausgewählt wird.                                             
                if state != TurnState.SELECT_STONE_TYPE and (boardX == -1 or boardY == -1):
                    continue     
                # Das Spiel arbeitet mit 3 Zuständen.                        
                match state:
                    # Zunächst wählt man den zu verschiebenden Stein aus.
                    case TurnState.SELECT_FROM_STONE:
                        gameMoveX = boardX
                        gameMoveY = boardY
                        state = TurnState.SELECT_TO_STONE    
                    # War die Aktion davor erfolgreich soll hier das Zielfeld für den zu verschiebenden Stein ausgewählt werden.                    
                    case TurnState.SELECT_TO_STONE:
                        # Wenn es sich um den ersten Zug handelt, wird der erste Zustand übersprungen. 
                        # Dies wird so gelöst, dass der zu platzierende Stein auf das ausgewählte Feld gestellt wird und das Feld vorher praktisch von sich selbst zu sich selbst verschiebt.
                        if turnNumber == 1:
                            gameMoveX = boardX
                            gameMoveY = boardY

                        gameMoveNewX = boardX
                        gameMoveNewY = boardY
                        #print(boardX, boardY)
                        state = TurnState.SELECT_STONE_TYPE
                    # Hier wird der Stein ausgewählt den man setzen möchte. Ist das Feld erfolgreich getroffen, hat man selber noch genug Steine übrig, wird der Zug erfolgreich ausgeführt. 
                    # Der Zustand ändert sich in jedem Fall wieder zum ersten State, egal ob der Zug erfolgreich war oder nicht.
                    case TurnState.SELECT_STONE_TYPE:                        
                        gameMoveStoneType = -1
                        index = 3
                        for hitbox in stoneIconHitboxes :
                            if mouseX >= hitbox[0] and mouseX <= (hitbox[0] + hitbox[2]) and mouseY >= hitbox[1] and mouseY <= (hitbox[1] + hitbox[3]):
                                gameMoveStoneType = index
                            index += 1
                        if gameMoveStoneType == -1:
                            continue
                        #gameMoveStoneType = random.randint(3, 11)

                        print(tuple([gameMoveX,gameMoveY,gameMoveNewX,gameMoveNewY,gameMoveStoneType]))
                        try:
                            action = game.moveCountMap[tuple([gameMoveX,gameMoveY,gameMoveNewX,gameMoveNewY,gameMoveStoneType])]
                        except:
                            state = TurnState.SELECT_FROM_STONE
                            continue
                        valids = game.getValidMoves(game.getCanonicalForm(board, curPlayer), curPlayer) # ,1
                        if valids[action] == 0:
                            state = TurnState.SELECT_FROM_STONE
                            continue
                        board, curPlayer = game.getNextState(board, curPlayer, action)
                        turnNumber += 1
                        game.display(board)
                        state = TurnState.SELECT_FROM_STONE           
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                running = False        
        elif(event.type == QUIT):
            running = False
    # Eigentlich war geplant, dass man mehrere Partien spielen kann, ohne das Spiel beenden zu müssen. 
    # Für diese Implementierung hat jedoch am Ende die Zeit nicht mehr gereicht.
    if game.getGameEnded(board, curPlayer) != 0:
        pass
        #game.restart()
    groupCounts = game.getGroupCounts(board)
    screen.fill((255, 255, 255))
    screen.blit(boardSprite.surf,BOARD_POS)
    # Spielfeld rendern
    for y in range(8):       
        for x in range(8):
            stoneType = board[x][y]
            stone : GameSprite = getStoneSpriteFromNum(stoneType)           
            if stone is None:
                continue
            screen.blit(stone.surf, (stoneSlotHitboxes[8 * y + x][4], stoneSlotHitboxes[8 * y + x][5] - stone.rect.height)) 

    # HUD rendern
    fontSurf = font.render("Color Player" if game.colorPlayer == 1 else "Height Player", True, (0,0,0))            
    screen.blit(fontSurf, (20, 20))

    # Wenn Spieler 1 gewinnt, soll dem Spieler mitgeteilt werden, dass er gewonnen hat.
    if game.getGameEnded(board, curPlayer) == 1:
        surface = pygame.Surface((160, 50))
        surface.fill((255,255,255))
        fontSurf = font.render("You won", True, (0,0,0), (255, 255, 255))            
        #screen.blit(surface, (625, 375))
        screen.blit(fontSurf, (625, 375))
    # Wenn Spieler 1 gewinnt, soll dem Spieler mitgeteilt werden, dass er verloren hat.
    elif game.getGameEnded(board, curPlayer) == -1:
        surface = pygame.Surface((160, 50))
        surface.fill((255,255,255))
        fontSurf = font.render("You lost", True, (0,0,0), (255, 255, 255))            
        #screen.blit(surface, (625, 375))
        screen.blit(fontSurf, (675, 375))
    # Wenn niemand gewinnt, soll dem Spieler mitgeteilt werden, dass das Spiel in einem Unentschieden geendet hat.
    else:
        surface = pygame.Surface((160, 50))
        surface.fill((255,255,255))
        fontSurf = font.render("Draw", True, (0,0,0), (255, 255, 255)) 

    zeroColors = 0
    for i in range(3):        
        if sum(game.colorPlayerPieces[i * 3 : i * 3 + 3]) == 9 and sum(game.heightPlayerPieces[i * 3 : i * 3 + 3]) == 9:
            fontSurf = scoreFont.render(colorStrings[i] + (" " * (len(colorStrings[0]) - len(colorStrings[i]))) + ":"  + str(0), True, (0,0,0))
            zeroColors += 1
        else:
            fontSurf = scoreFont.render(colorStrings[i] + (" " * (len(colorStrings[0]) - len(colorStrings[i]))) + ":"  + str(groupCounts["colorGroupMax"][i]), True, (0,0,0))
        screen.blit(fontSurf, (1000, 680 + i * 50))

    fontSurf = scoreFont.render("___", True, (0,0,0))
    screen.blit(fontSurf, (1295, 685 + 2 * 50))
    fontSurf = scoreFont.render(str(groupCounts["colorSum"] - zeroColors), True, (0,0,0))
    screen.blit(fontSurf, (1313, 680 + 3 * 50))

    zeroHeights = 0
    for i in range(3):   
        if game.colorPlayerPieces[i] + game.colorPlayerPieces[i + 3] + game.colorPlayerPieces[i + 6] == 9 and game.heightPlayerPieces[i] + game.heightPlayerPieces[i + 3] + game.heightPlayerPieces[i + 6] == 9:
            fontSurf = scoreFont.render(heightStrings[i] + (" " * (len(heightStrings[1]) - len(heightStrings[i]))) + ":"  + str(0), True, (0,0,0))
            zeroHeights += 1
        else:
            fontSurf = scoreFont.render(heightStrings[i] + (" " * (len(heightStrings[1]) - len(heightStrings[i]))) + ":"  + str(groupCounts["heightGroupMax"][i]), True, (0,0,0))
        screen.blit(fontSurf, (20, 680 + i * 50))

    fontSurf = scoreFont.render("___", True, (0,0,0))
    screen.blit(fontSurf, (170, 685 + 2 * 50))
    fontSurf = scoreFont.render(str(groupCounts["heightSum"] - zeroHeights), True, (0,0,0))
    screen.blit(fontSurf, (189, 680 + 3 * 50))

    for y in range(3):       
        for x in range(3):
            #stoneIcon = getStoneSpriteFromNum((3 * y + x) + 3)
            stoneIcon = stoneIcons[3 * y + x]
            #stoneIcon.surf = pygame.transform.scale_by(stoneIcon.surf, 0.5 - x * 0.1)
            fontSurf = font.render(str(game.colorPlayerPieces[3 * y + x]) if game.colorPlayer == 1 else str(game.heightPlayerPieces[3 * y + x]), True, (0,0,0))
            screen.blit(stoneIcon.surf, (1160 + x * 80, 20 + y * 50))
            screen.blit(fontSurf, (1200 + x * 80, 10 + y * 50))
    # screen.blit(fontSurf, (900, 10)) 
    # screen.blit(stone2.surf, (1200, 10))          
    pygame.display.flip()

pygame.quit()

