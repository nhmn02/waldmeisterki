from __future__ import print_function
import random
import sys

from utils import dotdict
sys.path.append('..')
from Game import Game
from .WaldmeisterBoard import Board
from bidict import bidict
import numpy as np

"""
Game class implementation for the game of Waldmeister.
Based on the OthelloGame then getGameEnded() was adapted to new rules.

Author: Nick Hamann & Lukas Lebruska, https://git.thm.de/nhmn02/waldmeisterki/
Date: Dez 6, 2023.

Based on the OthelloGame by Surag Nair.
"""
class WaldmeisterGame(Game):
    def __init__(self, n: int = 8, gameGoal: int = 1):
        self.n = n
        self.ACTION_SIZE = 11160
        self.colorPlayerPieces = [3] * 9
        self.heightPlayerPieces = [3] * 9
        self.colorPlayer = gameGoal # Spieler 1 ist standardmäßig der colorPlayer.
        self.enumerateMoves()
    
    # Erstelle Dictionairy von allen Zügen. Alle Züge sollen durchnummeriert werden.
    def enumerateMoves(self):
        moveCount = 0
        self.moveCountMap = bidict()
        for y in range(self.n):
            for x in range(self.n):
                # Erster Zug
                for stoneType in range(3, 12):
                    newmove = (x, y, x, y, stoneType)
                    self.moveCountMap[newmove] = moveCount
                    moveCount += 1

                # Horizontale Bewegung
                for newX in range(8):                                         
                    if(newX != x):
                        for stoneType in range(3, 12):
                            newmove = (x, y, newX, y, stoneType)
                            self.moveCountMap[newmove] = moveCount
                            moveCount += 1
                # Vertikale Bewegung
                for newY in range(8):
                    if(newY != y):
                        for stoneType in range(3, 12):
                            newmove = (x, y, x, newY, stoneType)
                            self.moveCountMap[newmove] = moveCount
                            moveCount += 1  
                # Diagonale Bewegung
                for newPos in range(1, 8):                   
                    if((x + newPos) < self.n and (y - newPos) >= 0):
                        for stoneType in range(3, 12):
                            newmove = (x, y, x + newPos, y - newPos, stoneType)
                            self.moveCountMap[newmove] = moveCount
                            moveCount += 1
                    if((x - newPos) >= 0 and (y + newPos) < self.n):
                        for stoneType in range(3, 12):
                            newmove = (x, y, x - newPos, y + newPos, stoneType)
                            self.moveCountMap[newmove] = moveCount
                            moveCount += 1


    def getInitBoard(self):
        # Gibt das initialisierte Spielbrett zurück (numpy board)
        b = Board(self.n)
        return np.array(b.pieces)

    def getBoardSize(self):
        # (a,b) Tupel
        return (self.n, self.n)

    def getActionSize(self):
        # Gibt die Anzahl der möglichen Aktionen zurück.  
        # Feldkombinationen 14 * 1 + 15 * 2 + 16 * 3 + 17 * 4 + 18 * 5 + 19 * 6 + 20 * 7 + 21 * 8 + 20 * 7 + 19 * 6 + 18 * 5 + 17 * 4 + 16 * 3 + 15 * 2 + 14 * 1 
        # = 1176  
        # Feldkombinationen * Farb/höhenkombinationen = 1176 * 9 = 10584
        # Erster Zug = 64 * 9 = 576
        # 576 + 10584 = 11.160
        return self.ACTION_SIZE 

    def getNextState(self, board : np.ndarray, player : int, action : int):
        # Führt die Aktion des Spielers aus und gibt das neue Spielbrett und den nächsten Spieler zurück, wobei jeder Zug legal sein muss.        
        b = Board(self.n)
        b.pieces = np.copy(board)
        move = self.moveCountMap.inverse[action]
        b.execute_move(move, self.colorPlayerPieces if self.colorPlayer == player else self.heightPlayerPieces)
        return (b.pieces, -player)
        

    def getValidMoves(self, board: np.ndarray, player : int):
        # Gibt eine Liste zurück, in der für jeden Zug 1 oder 0 steht, wobei 1 legal und 0 illegal bedeutet. 
        valids = [0]*self.getActionSize()
        b = Board(self.n)
        b.pieces = np.copy(board)
        legalMoves =  b.get_legal_moves(self.colorPlayerPieces if self.colorPlayer == player else self.heightPlayerPieces, self.isFirstTurn())
        for x in legalMoves:
             valids[self.moveCountMap[x]] = 1          
        return np.array(valids)        

    def getGameEnded(self, board : np.ndarray, player : int):
        # Gibt 0 zurück, wenn das Spiel noch nicht vorbei ist, 1 wenn der Spieler gewonnen hat, -1 wenn der Spieler verloren hat.
        # Endet das Spiel in einem Unentschieden, so soll ein "kleiner Wert" zurückgegeben werden (in diesem Fall 1e-4).
        b = Board(self.n)
        b.pieces = np.copy(board)

        if b.has_legal_moves(self.colorPlayerPieces, self.heightPlayerPieces):
            return 0
        return b.is_win(player == self.colorPlayer)
        #     return 1               
        # return -1

    def getCanonicalForm(self, board : np.ndarray, player : int):
        # Gibt das Spielbrett zurück. Der auskommentierte Quellcode war ein versuch das canonicalBoard einzubauen.
        # Wenn das canoncialBoard aufgerufen wird soll immer die Sicht des colorPlayer zu sehen sein. Wenn player gerade colorPlayer ist, soll sich nichts ändern.
        # Ist player jedoch gerade heightPlayer, so sollen sich die Steine auf dem canonicalBoard so ändern, dass die Ansicht der des colorPlayer entspricht. 
        # newBoard = np.copy(board)
        # if player != self.colorPlayer:
        #    for y in range(self.n):
        #        for x in range(self.n):
        #            match board[x][y]:
        #                case 4:
        #                    newBoard[x][y] = 6
        #                case 5:
        #                    newBoard[x][y] = 9
        #                case 6:
        #                    newBoard[x][y] = 4
        #                case 8:
        #                    newBoard[x][y] = 10
        #                case 9:
        #                    newBoard[x][y] = 5
        #                case 10:
        #                    newBoard[x][y] = 8

        # return newBoard
        return board

    def getSymmetries(self, board : np.ndarray, pi : list):
        # Der aus Othello übernommene Beispielcode funktioniert hier so nicht, weshalb der Code nicht verwendet worden ist.
        # mirror, rotational
        #  assert(len(pi) == self.n**2 + 1)
        #assert(len(pi) == self.getActionSize())  # 1 for pass
        # pi_board = np.reshape(pi[:-1], (self.n, self.n))
        # pi_board = np.reshape(pi, (self.n, self.n))
        # l = []

        # for i in range(1, 5):
        #     for j in [True, False]:
        #         newB = np.rot90(board, i)
        #         newPi = np.rot90(pi_board, i)
        #         if j:
        #             newB = np.fliplr(newB)
        #             newPi = np.fliplr(newPi)
        #         l += [(newB, list(newPi.ravel()) + [pi[-1]])]
        # return l
        return [(board, pi)]

    def stringRepresentation(self, board : np.ndarray):
        # Gibt das Spielbrett inklusive der noch ausspielbaren Figuren zurück.
        return str(board) + "\n" + str(self.colorPlayerPieces) + "\n" + str(self.heightPlayerPieces)

    def isFirstTurn(self):
        # Prüft ob es der erste Zug im Spiel ist. Dafür wird überprüft, ob beide Spieler noch all ihre Steine haben. Wenn True ist es der erste Zug.
        for piece in self.colorPlayerPieces:
            if piece < 3:
                return False
        for piece in self.heightPlayerPieces:
            if piece < 3:
                return False
        return True

    def getGroupCounts(self, board : np.ndarray):
        """        
        Input:
            board: Aktuelles Spielbrett
        Returns:
            Die Anzahl der größten zusammendhängenden Gruppen für Farben als auch für Höhen
        """
        # Wird im Training nur bei Spielende aufgerufen, in der GUI jedoch jeden Frame. 
        b = Board(self.n)
        b.pieces = np.copy(board)
        return b.countAllGroups()

    @staticmethod
    def display(board : np.ndarray):
        # Sorgt für die Konsolenausgabe des Spiels nach jedem Zug in der GUI.
        n = board.shape[0]
        print("   ", end="")
        for y in range(n):
            print(y, end=" ")
        print("")
        print("-----------------------")
        for y in range(n):
            print(y, "|", end="")    # print the row #
            for x in range(n):
                piece = board[y][x]    # get the piece to print
                print(piece, end=" ")
            print("|")

        print("-----------------------")
