'''
Klasse Board für das Spiel Waldmeister
Standardgröße ist 8x8
Board-Daten:
  3-5  = Gelbgrün
  6-8  = Blattgrün
  9-11 = Dunkelgrün
  3, 6, 9  = klein
  4, 7, 10 = mittel
  5, 8, 11 = groß

Author: Nick Hamann & Lukas Lebruska
Date: Jan 22, 2024.

Based on the board for the game of Othello by Eric P. Nichols.

'''
from utils import dotdict


class Board():

    # Alle Richtungen, die auf dem Spielbrett erlaubt sind, d.h. alle außer der einen Diagonle, als (x, y) Offset.   
    __directions = [(1,-1),(1,0),(0,-1),(-1,1),(-1,0),(0,1)]

    def __init__(self, n : int = 8):
        "Erzeugt leeres Spielbrett"

        self.n = n        
        self.pieces = [None]*self.n
        for i in range(self.n):
            self.pieces[i] = [0]*self.n        
                   
    # Füge [][] Syntax zum Spielbrett hinzu.
    def __getitem__(self, index : int): 
        return self.pieces[index]

    def get_legal_moves(self, playerGamePieces : list, isFirstTurn: bool):
        """
        Gibt eine Liste mit allen legalen Zügen als 5 Tupel, für den Spieler mit den übergebenen Spielsteinen zurück.
        Das 5 Tupel ist so aufgebaut (x Position, y Position, neue x Position, neue y Position, Art des Spielsteins).
        Der Stein wird also von der ursprünglichen Position zur Neuen verschoben und durch den neuen Spielstein ersetzt.
        """
        moves = set()  # Speichert die legalen Züge.

        for y in range(self.n):
            for x in range(self.n):
                # Erster Zug: für jedes Feld x y x y Stein
                if(isFirstTurn):
                    self.addMovesForCell(x, y, x, y, playerGamePieces, moves)                                             
                else:
                    if self[x][y] != 0:                    
                        # Horizontale Bewegung in echt bzw. Vertikale Bewegung auf der Konsole.

                        for newX in range(x + 1, 8):
                            # Anderer Stein blockiert => Zug ist invalide
                            if(self.pieces[newX][y] != 0):
                                break
                            self.addMovesForCell(x, y, newX, y, playerGamePieces, moves)
                          

                        for newX in range(x - 1, -1, -1):
                            # Anderer Stein blockiert => Zug ist invalide
                            if(self.pieces[newX][y] != 0):
                                break
                            self.addMovesForCell(x, y, newX, y, playerGamePieces, moves)

                        # Vertikale Bewegung                            
                        for newY in range(y + 1, 8):
                            # Anderer Stein blockiert => Zug ist invalide
                            if(self.pieces[x][newY] != 0):
                                break
                            self.addMovesForCell(x, y, x, newY, playerGamePieces, moves)                          

                        for newY in range(y - 1, -1, -1):
                            # Anderer Stein blockiert => Zug ist invalide
                            if(self.pieces[x][newY] != 0):
                                break
                            self.addMovesForCell(x, y, x, newY, playerGamePieces, moves)


                        # Diagonale Bewegung
                        for newPos in range(1, 8):
                            # Noch im Feld
                            if((x + newPos) < self.n and (y - newPos) >= 0):
                                # Anderer Stein blockiert => Zug ist invalide
                                if(self.pieces[x + newPos][y - newPos] != 0):
                                    break
                                self.addMovesForCell(x, y, x + newPos, y - newPos, playerGamePieces, moves)                               
                    
                        for newPos in range(1, 8):  
                            # Noch im Feld           
                            if((x - newPos) >= 0 and (y + newPos) < self.n):
                                # Anderer Stein blockiert => Zug ist invalide
                                if(self.pieces[x - newPos][y + newPos] != 0):
                                    break
                                self.addMovesForCell(x, y, x - newPos, y + newPos, playerGamePieces, moves)  
                      
        return list(moves)

    def addMovesForCell(self, x : int, y : int, newX : int, newY : int, playerGamePieces : list, moves : set):
        """
        Hilfsfunktion die für jedes Positionenpaar alle möglichen Spielsteintypen ergänzt und in die moves-Menge einfügt.
        """       
        for stoneType in range(3, 12):
            if(playerGamePieces[stoneType - 3] > 0):
                newmove = (x, y, newX, newY, stoneType)
                moves.add(newmove)

    def has_legal_moves(self, colorPlayerPieces : list, heightPlayerPieces : list):
        """
        Das Spiel soll laufen so lange laufen bis kein Spieler mehr Steine zum Setzen übrig hat.
        """
        for piece in colorPlayerPieces:
            if piece > 0:
                return True
        for piece in heightPlayerPieces:
            if piece > 0:
                return True
        return False

    
    def is_win(self, colorPlayer : bool):
        """
        Gibt zurück ob der übergebenene Spieler (Farbe oder Höhe) gewonnen hat.
        """              
        counts = self.countAllGroups()
        colorSum = counts["colorSum"]
        heightSum = counts["heightSum"]

        if(colorPlayer) == True:
            if colorSum > heightSum:
                return 1
            elif colorSum < heightSum:
                return -1
            return 1e-4
        
        if heightSum > colorSum:
            return 1
        elif heightSum < colorSum:
            return -1
        return 1e-4
        #return heightSum > colorSum

    def countAllGroups(self):
        """
        Gibt für alle Höhen- und Farbgruppen, die größte Gruppe sowie deren Summe zurück.
        """
        colorGroupMax = [1] * 3
        colorSum : int 
        visited = [None] * self.n
        for i in range(self.n):
            visited[i] = [False] * self.n 

        for y in range(self.n):
            for x in range(self.n):
                # Ignoriere leere Felder
                if(self[x][y] == 0 or visited[x][y] == True):
                    continue
                color = (self[x][y] // 3) - 1
                colorGroupMax[color] = max(self.countGroup(x, y, color, True, visited), colorGroupMax[color])
        colorSum = sum(colorGroupMax)
       
        heightGroupMax = [1] * 3
        heightSum : int 
        visited = [None] * self.n
        for i in range(self.n):
            visited[i] = [False] * self.n 

        for y in range(self.n):
            for x in range(self.n):
                # Ignoriere leere Felder
                if(self[x][y] == 0 or visited[x][y] == True):
                    continue
                height = self[x][y] % 3
                heightGroupMax[height] = max(self.countGroup(x, y, height, False, visited), heightGroupMax[height])    
        heightSum = sum(heightGroupMax)

        return {"colorGroupMax" : colorGroupMax, "colorSum" : colorSum, "heightGroupMax" : heightGroupMax, "heightSum" : heightSum}

    def countGroup(self, x : int, y : int, colorOrHeight : int, checkColor: bool, visited) -> int:
        """
        Gibt die Größe der Gruppe, von der Position zurück.
        Basiert im Kern auf der Breitensuche.
        """
        groupCount = 0
        queue = []
        queue.append((x, y))
        visited[x][y] = True

        while queue:
            s = queue.pop(0)
            groupCount += 1
            for xOffset, yOffset in self.__directions:
            # Ist Nachbar noch im Spielfeld
                newX = s[0] + xOffset
                newY = s[1] + yOffset
                if newX >= 0 and newX < self.n and newY >= 0 and newY < self.n:
                    if visited[newX][newY] == False:
                        if checkColor == True:                    
                            if (self[newX][newY] // 3) - 1 == colorOrHeight:
                                queue.append((newX,newY))
                                visited[newX][newY] = True                               
                        else:
                            if self[newX][newY] != 0 and self[newX][newY] % 3 == colorOrHeight:
                                queue.append((newX,newY))
                                visited[newX][newY] = True               
        return groupCount

    def execute_move(self, move : tuple, playerGamePieces : list):
        """
        Führt den im move-Tupel kodierten Zug aus. vgl. get_legal_moves
        """
        # move = zahl => rückwärts aus map lesen
        self[move[2]][move[3]] = self[move[0]][move[1]]
        self[move[0]][move[1]] = move[4]        
        playerGamePieces[move[4] - 3] -= 1    # Der ausgespielte Stein wird aus den noch verfügbaren Steinen des Spielers entfernt.   

