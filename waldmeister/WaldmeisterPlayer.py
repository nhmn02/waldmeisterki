import numpy as np


class RandomPlayer():
    def __init__(self, game):
        self.game = game

    def play(self, board):
        a = np.random.randint(self.game.getActionSize())
        valids = self.game.getValidMoves(board, -1)
        while valids[a]!=1:
            a = np.random.randint(self.game.getActionSize())
        return a


class HumanWaldmeisterPlayer():
    def __init__(self, game):
        self.game = game

    def play(self, board):
        # display(board)
        valid = self.game.getValidMoves(board, 1)
        for i in range(len(valid)):
            if valid[i]:
                pass
                #print("[", self.game.moveCountMap.inverse[i], end="] ")
        while True:
            input_move = input()
            input_a = input_move.split(" ")
            if len(input_a) == 5:
                try:
                    x,y,newX,newY,stone = [int(i) for i in input_a]
                    #if ((0 <= x) and (x < self.game.n) and (0 <= y) and (y < self.game.n)) or \
                    #        ((x == self.game.n) and (y == 0)):
                    #     a = self.game.n * x + y if x != -1 else self.game.n ** 2
                    #    self.game.moveCountMap[tuple(input_a)]
                    #if self.game.getValidMoves(board, 1) [self.game.moveCountMap[tuple([x, y, newX, newY, stone])]] == 1: 
                    #    break
                    # if valid[a]:
                    #     break
                    break
                except ValueError:
                    # Input needs to be an integer
                    'Invalid integer'
            print('Invalid move')
        return self.game.moveCountMap[tuple([x,y,newX,newY,stone])]