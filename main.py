import logging

import coloredlogs

from Coach import Coach
from waldmeister.WaldmeisterGame import WaldmeisterGame
from waldmeister.WaldmeisterPlayer import *
from waldmeister.keras.NNet import NNetWrapper as nn
# from tictactoe.TicTacToeGame import TicTacToeGame as WaldmeisterGame
# from tictactoe.TicTacToePlayers import *
# from tictactoe.keras.NNet import NNetWrapper as nn
from utils import *

log = logging.getLogger(__name__)

coloredlogs.install(level='INFO')  # Für mehr Informationen aendere 'INFO' zu 'DEBUG'

args = dotdict({
    'numIters': 20,             # Anzahl der Iterationen für das Training
    'numEps': 50,               # Anzahl der zu simuliernden self-play Spielen pro Iteration (self-play Spiele im self-play)
    'tempThreshold': 15,        #
    'updateThreshold': 0.55,    # Schlägt neue KI in Arena playoff Spielen alte KI mindestens um diesen Threshold (hier 55%), wird das Neuronale Netz akezeptiert
    'maxlenOfQueue': 200000,    # Maximale Anzahl an Beispiel-Spielen zum Trainieren des Neuronalen Netz
    'numMCTSSims': 25,          # Anzahl an Spielzuegen die vom MCTS simuliert werden sollen
    'arenaCompare': 20,         # Anzahl der Spiele die in der Arena zwischen der new und prev KI ausgetragen werden sollen
    'cpuct': 3,                 # Wartezeit

    'checkpoint': './temp/',
    'load_model': True,
    'load_folder_file': ('./temp','checkpoint_1.pth.tar'),
    'numItersForTrainExamplesHistory': 40,

})


def main():
    log.info('Loading %s...', WaldmeisterGame.__name__)
    g = WaldmeisterGame()

    log.info('Loading %s...', nn.__name__)
    nnet = nn(g)

    if args.load_model:
        log.info('Loading checkpoint "%s/%s"...', args.load_folder_file[0], args.load_folder_file[1])
        nnet.load_checkpoint(args.load_folder_file[0], args.load_folder_file[1])
    else:
        log.warning('Not loading a checkpoint!')

    log.info('Loading the Coach...')
    c = Coach(g, nnet, args)

    if args.load_model:
        log.info("Loading 'trainExamples' from file...")
        c.loadTrainExamples()

    log.info('Starting the learning process 🎉')
    c.learn()

if __name__ == "__main__":
    main()
